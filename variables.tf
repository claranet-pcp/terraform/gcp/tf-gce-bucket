variable "bucket_name"{
  description = "The name of the storage bucket you wish to create"
  type = "string"
}

variable "location"{
  description = "The location for the storage bucket, can be regional or multi-regional"
  type = "string"
}
