resource "google_storage_bucket" "bucket" {
  name     = "${var.bucket_name}"
  location = "${var.location}"
}

resource "google_storage_bucket_acl" "acl" {
  bucket = "${google_storage_bucket.bucket.name}"
}
